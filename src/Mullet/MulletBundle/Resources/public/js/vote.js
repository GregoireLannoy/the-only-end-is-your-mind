$('.voteButton').click( function(){

	var vote = ($(this).attr('id') == 'positiveButton') ? 'positive' : 'negative';
	var id   = $(this).attr('data-id');
	var positiveCount = $('#positiveCount').html();
	var negativeCount = $('#negativeCount').html();

	$.ajax({
		type: 'GET',
		url: 'vote/'+vote+'/'+id,
		})
		.done(function(html) {
			$('#'+vote+'Count').html(parseInt($('#'+vote+'Count').html())+1); 
		});
 });