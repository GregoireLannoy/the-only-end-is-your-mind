<?php

namespace Mullet\MulletBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Chapter
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mullet\MulletBundle\Entity\ChapterRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Chapter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Chapter", mappedBy="parents", cascade={"persist"})
     * @ORM\OrderBy({"positive"="desc"})
     */
    private $childs;

    /**
     * @ORM\ManyToMany(targetEntity="Chapter", inversedBy="childs")
     * @ORM\JoinTable(name="family",
     *      joinColumns={@ORM\JoinColumn(name="child_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="parent_id", referencedColumnName="id")}
     *      )
     */
    private $parents;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="text")
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $img;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var integer
     *
     * @ORM\Column(name="positive", type="integer")
     */
    private $positive;

    /**
     * @var integer
     *
     * @ORM\Column(name="negative", type="integer")
     */
    private $negative;

    /**
     * @var boolean
     *
     * @ORM\Column(name="nsfw", type="boolean")
     */
    private $nsfw;

    /**
     * @var datetime
     *
     * @ORM\Column(name="updateDate", type="datetime")
     */
    private $updateDate;


    public function __construct() {
        $this->parents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->childs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return Chapter
     */
    public function setAction($action)
    {
        $this->action = $action;
    
        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Chapter
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set img
     *
     * @param string $img
     * @return Chapter
     */
    public function setImg($img)
    {
        $this->img = $img;
    
        return $this;
    }

    /**
     * Get img
     *
     * @return string 
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set path
     *
     * @param string $img
     * @return Chapter
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set positive
     *
     * @param integer $positive
     * @return Chapter
     */
    public function setPositive($positive)
    {
        $this->positive = $positive;
    
        return $this;
    }

    /**
     * Get positive
     *
     * @return integer 
     */
    public function getPositive()
    {
        return $this->positive;
    }

    /**
     * Set negative
     *
     * @param integer $negative
     * @return Chapter
     */
    public function setNegative($negative)
    {
        $this->negative = $negative;
    
        return $this;
    }

    /**
     * Get negative
     *
     * @return integer 
     */
    public function getNegative()
    {
        return $this->negative;
    }

    /**
     * Set nsfw
     *
     * @param boolean $nsfw
     * @return Chapter
     */
    public function setNsfw($nsfw)
    {
        $this->nsfw = $nsfw;
    
        return $this;
    }

    /**
     * Get nsfw
     *
     * @return boolean 
     */
    public function getNsfw()
    {
        return $this->nsfw;
    }

    /**
     * Add parent
     *
    */
    public function addParent(Chapter $parent)
    {
        $this->parents[] = $parent;
        $parents->childs[] = $this; // php allows to access private members of objects of the same type
    }

    /**
     * Remove parent
     *
     * @param Chapter
    */
    public function removeParent(Chapter $parent)
    {
        $this->parents->removeElement($parent);
    }

    /**
     * Get parents
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * Add child
     *
    */
    public function addChild(Chapter $child)
    {
        $this->childs[] = $child;
        $childs->parents[] = $this; // php allows to access private members of objects of the same type
    }

    /**
     * Remove child
     *
     * @param Chapter
    */
    public function removeChild(Chapter $child)
    {
        $this->childs->removeElement($child);
    }

    /**
     * Get childs
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getChilds()
    {
        return $this->childs;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->img) {
            // faites ce que vous voulez pour générer un nom unique
            $this->path = sha1(uniqid(mt_rand(), true)).'.'.$this->img->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->img) {
            return;
        }

        // s'il y a une erreur lors du déplacement du fichier, une exception
        // va automatiquement être lancée par la méthode move(). Cela va empêcher
        // proprement l'entité d'être persistée dans la base de données si
        // erreur il y a
        $this->img->move($this->getUploadRootDir(), $this->path);

        unset($this->img);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // le chemin absolu du répertoire où les documents uploadés doivent être sauvegardés
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // on se débarrasse de « __DIR__ » afin de ne pas avoir de problème lorsqu'on affiche
        // le document/image dans la vue.
        return 'uploads';
    }

    public function votePositive(){
        $this->setPositive($this->getPositive()+1);

        return $this;
    }

    public function voteNegative(){
        $this->setNegative($this->getNegative()+1);

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     *
     * @return Chapter
     */
    public function setUpdateDate()
    {
        $this->updateDate = new \DateTime();

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return date
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    public function getRatio(){
        if($this->negative == 0) return $this->positive;
        elseif($this->positive == 0) return 0;
        else return $this->positive/$this->negative;
    }
}