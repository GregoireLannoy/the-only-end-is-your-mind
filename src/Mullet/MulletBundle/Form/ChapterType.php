<?php

namespace Mullet\MulletBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChapterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('action'  ,'text', array(
                'attr' => array(
                    'placeholder' => 'I...'
                    )
                )
            )
            ->add('text'    ,'textarea', array(
                'attr' => array(
                    'placeholder' => 'You...'
                    )
                )
            )
            ->add('img'     ,'file', array(
                'required' => $options['imgIsRequired']
                )
            )
            ->add('nsfw'    ,'checkbox', array(
                'required' => false
                )
            )
            ->add('save'    ,'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'Mullet\MulletBundle\Entity\Chapter',
            'imgIsRequired' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mullet_mulletbundle_chapter';
    }
}
