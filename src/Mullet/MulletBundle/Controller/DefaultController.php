<?php

namespace Mullet\MulletBundle\Controller;

use Mullet\MulletBundle\Entity\Chapter;
use Mullet\MulletBundle\Form\ChapterType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction($id)
    {
    	$chapters = $this->getDoctrine()->getRepository('MulletMulletBundle:Chapter');

    	$actualChapter   = $chapters->find($id);
    	$chapterChildren = $actualChapter->getChilds($id);

        $chapterChildrenByPositiveDesc = $chapterChildren->getIterator();
        $chapterChildrenByPositiveDesc->uasort(function ($first, $second) {
            return $first->getPositive() < $second->getPositive();
        });

        $chapterChildrenByRatioDesc = $chapterChildren->getIterator();
        $chapterChildrenByRatioDesc->uasort(function ($first, $second) {
            return $first->getRatio() < $second->getRatio();
        });

        $chapterChildrenByDateDesc = $chapterChildren->getIterator();
        $chapterChildrenByDateDesc->uasort(function ($first, $second) {
            return $first->getUpdateDate() > $second->getUpdateDate();
        });

    	if (!$actualChapter) {
	        throw $this->createNotFoundException(
	            'Actual chapter doesn\'t exists'
	        );
	    }

        return $this->render('MulletMulletBundle:Default:index.html.twig', array(
        	'chapter'=>$actualChapter,
        	'chapterChildrenByPositiveDesc'=>$chapterChildrenByPositiveDesc,
            'chapterChildrenByRatioDesc'=>$chapterChildrenByRatioDesc,
            'chapterChildrenByDateDesc'=>$chapterChildrenByDateDesc,
            'ratio'=>$actualChapter->getRatio()
        ));
    }

    public function createAction($parentId)
    {
    	if(!$parentId){
    		throw $this->createNotFoundException(
    			'No parent?'
    		);
    	}

       

    	$chapter = new Chapter();
    	$chapter->setPositive(0);
    	$chapter->setNegative(0);
        $chapter->setNsfw(false);

    	$form = $this->createForm(new ChapterType, $chapter);

    	if ($this->getRequest()->isMethod('POST')) {
	        $form->bind($this->getRequest());
	        if ($form->isValid()) {
	            $em = $this->getDoctrine()->getManager();

                $chapters        = $this->getDoctrine()->getRepository('MulletMulletBundle:Chapter');
                $parentChapter   = $chapters->find($parentId);
                $chapter->addParent($parentChapter);

	            $em->persist($chapter);
	            
                $em->flush();

	            return $this->redirect($this->generateUrl('mullet_mullet_homepage', array('id'=>$chapter->getId())));
	        }
	    }
    	
    	return $this->render('MulletMulletBundle:Default:createForm.html.twig', array(
    			'form'=>$form->createView(),
    			'parent'=>$parentId
    		));
    }

    public function editAction($chapterId)
    {
        if(!$chapterId){
            throw $this->createNotFoundException(
                'No chapter to edit'
            );
        }

        $em      = $this->getDoctrine()->getManager();
        $chapter = $em->getRepository('MulletMulletBundle:Chapter')->find($chapterId);

        if (!$chapter) {
            throw $this->createNotFoundException(
                'No chapter with this id: '.$id
            );
        }

        $form = $this->createForm(new ChapterType, $chapter, array('imgIsRequired' => false));

        if ($this->getRequest()->isMethod('POST')) {
            $form->bind($this->getRequest());
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $chapter->setUpdateDate();

                $em->persist($chapter);
                
                $em->flush();

                return $this->redirect($this->generateUrl('mullet_mullet_homepage', array('id'=>$chapter->getId())));
            }
        }

        return $this->render('MulletMulletBundle:Default:editForm.html.twig', array(
                'form'=>$form->createView()
            ));
    }

    public function voteAction($vote,$id){
    	if(!$id){
    		throw $this->createNotFoundException(
    			'Vote for who?'
    		);
    	}

    	$em      = $this->getDoctrine()->getManager();
    	$chapter = $em->getRepository('MulletMulletBundle:Chapter')->find($id);

    	if (!$chapter) {
	        throw $this->createNotFoundException(
	            'No chapter with this id: '.$id
        	);
        }

	    if($vote == 'positive') $chapter->votePositive();
	    elseif($vote == 'negative') $chapter->voteNegative();

	    $em->flush();

	    return new Response('OK!');
	    //return $this->redirect($this->generateUrl('mullet_mullet_homepage', array('id'=>$id)));
    }
}
