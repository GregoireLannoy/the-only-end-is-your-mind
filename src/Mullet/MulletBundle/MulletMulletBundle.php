<?php

namespace Mullet\MulletBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MulletMulletBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
}
